package com.kandy.smart.trace;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kandy on 18/8/17.
 */
public class TraceThread {
    private static Map<Thread,TraceContext> traceThreadMap = new HashMap<Thread, TraceContext>();

    public static TraceContext getTraceThreadData(Thread thread){
        if (null == traceThreadMap.get(thread)){
            return new TraceContext();
        }
        return traceThreadMap.get(thread);
    }

    public static void set(Thread thread,TraceContext traceThreadData){
        traceThreadMap.put(thread,traceThreadData);
    }
}
