package com.kandy.smart.trace;

/**
 * Created by kandy on 18/8/17.
 */
public class TraceContext {

    private String threadId;        //线程ID
    private String traceId;         //主链ID
    private String parentId;        //父ID,谁在调用我?
    private String appId;           //本应用ID
    private String localIp;         //本地Ip
    private String remoteIp;        //远程IP

    public String getThreadId() {
        return threadId;
    }

    public void setThreadId(String threadId) {
        this.threadId = threadId;
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getLocalIp() {
        return localIp;
    }

    public void setLocalIp(String localIp) {
        this.localIp = localIp;
    }

    public String getRemoteIp() {
        return remoteIp;
    }

    public void setRemoteIp(String remoteIp) {
        this.remoteIp = remoteIp;
    }
}
