package com.kandy.smart.trace.utils;

import java.util.Random;
import java.util.UUID;

/**
 * Created by kandy on 18/8/20.
 */
public class IdGenUtil {

    /**
     * 获取uuid串
     * @return
     */
    public static String getUUID(){
        return UUID.randomUUID().toString().replaceAll("-","").toLowerCase();
    }

    /**
     * 获取随机字符串(randomCount >= 3)
     * @param randomCount
     * @return
     */
    public static String getRandomGen(int randomCount){
        if (randomCount < 3){
            throw new RuntimeException("randomCount不能小于3");
        }

        char[] chars={'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9'};
        Random random=new Random();
        int count=0;

        StringBuilder sb=new StringBuilder();//动态字符串
        while(true){
            char c = chars[random.nextInt(chars.length)];
            if(sb.indexOf(c + "") == -1){
                sb.append(c);
                count++;
                if(count == randomCount){
                    break;
                }
            }
        }
        return sb.toString();
    }

    public static String getRandomGen4(){
        return getRandomGen(4);
    }

    public static String getRandomGen6(){
        return getRandomGen(6).toLowerCase();
    }

    public static String getRandomGen8(){
        return getRandomGen(8).toLowerCase();
    }
}
