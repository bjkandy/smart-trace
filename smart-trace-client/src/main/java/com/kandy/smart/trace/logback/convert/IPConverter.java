package com.kandy.smart.trace.logback.convert;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import com.kandy.smart.trace.utils.IPUtil;

/**
 * Created by kandy on 18/8/27.
 */
public class IPConverter extends ClassicConverter {
    @Override
    public String convert(ILoggingEvent iLoggingEvent) {
        try {
            return IPUtil.getLocalHostIp();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}