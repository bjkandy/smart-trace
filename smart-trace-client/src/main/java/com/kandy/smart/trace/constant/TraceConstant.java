package com.kandy.smart.trace.constant;

/**
 * Created by kandy on 18/8/17.
 */
public class TraceConstant {
    public static final String TRACE_ID = "trace_id";               //主链ID
    public static final String PARENT_ID = "parent_Id";            //父ID,谁在调用我?
    public static final String CHILD_ID = "child_Id";              //本应用ID
    public static final String LOCAL_IP = "local_ip";               //本地IP
    public static final String REMOTE_IP = "remote_ip";             //远程IP
}
